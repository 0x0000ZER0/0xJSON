# 0xJSON

A json parser written in `C`.

### API

- parse the JSON from a string:

```c
z0_json root;
z0_json_parse(MY_STRING, &root);

// ...

z0_json_free(&root);
```
- parse the JSON from a file:

```c
z0_json root;
z0_json_parse_file(MY_PATH, &root);

// ...

z0_json_free(&root);
```

- iterate over the items:

```c
void
iter(z0_json *root) {
        printf("VALUE: %s", root->val);

        for (uint32_t i = 0; i < root->len; ++i) 
                iter(root->list[i]);
}
```

- print the parsed JSON:

```c
z0_json_print(&root);
```
