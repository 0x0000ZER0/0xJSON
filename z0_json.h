#ifndef Z0_JSON_H
#define Z0_JSON_H

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdint.h>

#define Z0_JSON_KEY_MAX 64
#define Z0_JSON_VAL_MAX 64

typedef struct z0_json_s z0_json;
struct z0_json_s {
	uint8_t   key[Z0_JSON_KEY_MAX];
	uint8_t   val[Z0_JSON_VAL_MAX];

	uint32_t  cap;
	uint32_t  len;
	z0_json  *list;
};

void
z0_json_parse(uint8_t*, z0_json*);

void
z0_json_parse_file(uint8_t*, z0_json*);

void
z0_json_print(z0_json*);

void
z0_json_free(z0_json*);

#endif
