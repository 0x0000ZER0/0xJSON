#include "z0_json.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static uint8_t*
z0_json_parse_array(uint8_t *cursor, z0_json *root);

static uint8_t*
z0_json_parse_object(uint8_t *cursor, z0_json *root);

// ===========================================================
// ===================== IMPLEMENTATION ======================
// ===========================================================

static uint8_t*
z0_json_parse_array(uint8_t *cursor, z0_json *root) {
	root->cap  = 4;
	root->len  = 0;
	root->list = malloc(root->cap * sizeof (z0_json));

	if (*cursor == '[')
		++cursor;

	z0_json *curr;

	while (*cursor != '\0' && *cursor != ']') {
		if (root->len == root->cap) {
			root->cap  *= 2;
			root->list  = realloc(root->list, root->cap * sizeof (z0_json));
		}

		curr = &root->list[root->len];

		curr->key[0] = '\0';
		curr->val[Z0_JSON_VAL_MAX - 1] = '\0';
		curr->cap  = 4;
		curr->len  = 0;
		curr->list = malloc(curr->cap * sizeof (z0_json));

		while (*cursor == ' ' || *cursor == '\n' || *cursor == '\t')
			++cursor;

		uint8_t *temp;

		if (*cursor == '{') {
			cursor = z0_json_parse_object(cursor, curr);
		} else if (*cursor == '[') {
			cursor = z0_json_parse_array(cursor, curr);
		} else {
			temp = curr->val;
			while (*cursor != ','  && *cursor != '\n' && *cursor != ']' &&  *cursor != '\0' && *temp   != '\0') {
				*temp = *cursor;

				++temp;
				++cursor;
			}	
			*temp = '\0';
		}

		++root->len;

		if (*cursor == ']') {
			++cursor;
			break;
		}

		++cursor;
        }

	return cursor;
}

static uint8_t*
z0_json_parse_object(uint8_t *cursor, z0_json *root) {
	root->cap    = 4;
	root->len    = 0;
	root->list   = malloc(root->cap * sizeof (z0_json));

	if (*cursor == '{')
		++cursor;

	z0_json *curr;

	while (*cursor != '\0' && *cursor != '}') {
		if (root->len == root->cap) {
			root->cap  *= 2;
			root->list  = realloc(root->list, root->cap * sizeof (z0_json));
		}

		curr = &root->list[root->len];

		curr->key[Z0_JSON_KEY_MAX - 1] = '\0';
		curr->val[Z0_JSON_VAL_MAX - 1] = '\0';
		curr->cap  = 4;
		curr->len  = 0;
		curr->list = malloc(curr->cap * sizeof (z0_json));

		while (*cursor == ' ' || *cursor == '\n' || *cursor == '\t')
			++cursor;

		if (*cursor == '\"') {
			++cursor;

			uint8_t *temp;

			temp = curr->key;
			while (*cursor != '\"' && *cursor != '\0' && *temp != '\0') {
				*temp = *cursor;

				++temp;
				++cursor;			
			}
			*temp = '\0';

			++cursor;
			if (*cursor == ':')
				++cursor;

			while (*cursor == ' ')
				++cursor;

			if (*cursor == '{') {
				cursor = z0_json_parse_object(cursor, curr);
			} else if (*cursor == '[') {
				cursor = z0_json_parse_array(cursor, curr);
			} else {
				temp = curr->val;
				while (*cursor != ','  && *cursor != '\n' && *cursor != '}' &&  *cursor != '\0' && *temp   != '\0') {
					*temp = *cursor;

					++temp;
					++cursor;
				}	
				*temp = '\0';
			}

			++root->len;
		} 

		if (*cursor == '}') {
			++cursor;
			break;
		}

		++cursor;
        }

	return cursor;
}

void
z0_json_parse(uint8_t *json, z0_json *root) {
	z0_json_parse_object(json, root);
}

void
z0_json_parse_file(uint8_t *path, z0_json *root) {
	FILE *file;
	file = fopen(path, "r");
	if (file == NULL)
		return;

	fseek(file, 0, SEEK_END);
	
	size_t len;
	len = ftell(file);
	if (len == ULONG_MAX) {
		fclose(file);
		return;
	}

	fseek(file, 0, SEEK_SET);

	uint8_t *buff;
	buff = malloc(len + 1);

	if (buff != NULL) {
		fread(buff, sizeof (uint8_t), len, file);
		buff[len] = '\0';

		z0_json_parse(buff, root);
	}

	free(buff);
	fclose(file);
}

void
z0_json_print(z0_json *obj) {
	printf("{");

	for (uint32_t i = 0; i < obj->len; ++i) {
		if (obj->list[i].key[0] != '\0')
			printf("\"%s\":", obj->list[i].key);

		if (obj->list[i].len > 0)
			z0_json_print(&obj->list[i]);
		else 
			printf("%s", obj->list[i].val);

		if (i != obj->len - 1)
			printf(",");
	}

	printf("}");
}

void
z0_json_free(z0_json *root) {
	for (uint32_t i = 0; i < root->len; ++i)
		z0_json_free(&root->list[i]);

	free(root->list);
}
